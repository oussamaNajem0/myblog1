const express = require("express")
const bodyParser = require("body-parser")

app = express()


app.set("view engine", "pug")
// app.set("views", "views")
app.set('views', __dirname+'/views');

app.use(bodyParser.json({ type: 'application/json' }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static("public"))

app.get("/", (req, res) => {
    res.render("index")
})

// app.use((req, res)=>{
//     res.render("index")
// })

// app.get("/post/:id",(req, res)=>{
//     res.render("index")
// })

// app.delete("/post/:id",(req, res)=>{
//     res.render("index")
// })
app.use((req, res)=>{
    res.render("addpost")
})
app.get("/post/add",(req, res)=>{
    res.render("addpost")
})

app.post("/post/add",(req, res)=>{
    res.json(req.body)
})

app.use((req, res) => {
    res.end("<h1>Not found<h1>")
})


app.listen(3000)

